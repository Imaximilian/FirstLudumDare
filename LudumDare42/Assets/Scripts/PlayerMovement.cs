﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    Animator animChar;
    public float speed;
    float InputHorizontal;
    float Inputvertical;
    float inputFire;

	// Use this for initialization
	void Start () {
        animChar = this.gameObject.GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {

        InputHorizontal = Input.GetAxis("Horizontal");
        Inputvertical = Input.GetAxis("Vertical");
        animChar.SetFloat("InputHorizontal", InputHorizontal);
        animChar.SetFloat("InputVertical", Inputvertical);




    }
}
