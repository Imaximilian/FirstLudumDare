﻿using System.Collections;
using System.Collections.Generic;
using Unity.Transforms;
using Unity.Rendering;

using UnityEngine.UI;
using UnityEngine;
using TMPro;

class GameManager : MonoBehaviour
{
    public float enemyspawn;
    public float tileDestroyTimer;
    [SerializeField]
    private List<GameObject> tileArray = new List<GameObject>();
    //wir nun mit allen TIles befüllt 
    // wenn deie map procedural erstellt wird werden darin die Tiles gespeichert

    public TextMeshProUGUI TileDestroyCountdown;
    public TextMeshProUGUI TileDestroyInfo;
    public GameObject Canvas;

    private void Start()
    {
        TileDestroyInfo.enabled = false;
        TileDestroyCountdown.enabled = false;
        Canvas.SetActive(false);
        StartCoroutine(StartTimer());
        //var rnd = Random.Range(1,entityArray.Length); // 16 anpassen auf arraygröße

        #region 5x5 Grid
        //das sollte eigentlich ein 5x5 feld bauen

        //var tileArchetype = entityManager.CreateArchetype(
        //    typeof(TransformMatrix),
        //    typeof(Position),
        //    typeof(MeshInstanceRenderer),
        //    typeof(Tile)
        //);

        //for (int x = -3; x < 2; x++)
        //{
        //    for (int y = -3; y < 2; y++)
        //    {
        //        var tile = entityManager.CreateEntity(tileArchetype);

        //        entityManager.SetSharedComponentData(tile, new MeshInstanceRenderer
        //        {
        //            mesh = BlockMesh,
        //            material = BlockMaterial
        //        });


        //        entityManager.SetComponentData(tile, new Position { Value = new float3(x, y, 0) });
        //    }
        //}
        #endregion
    }

    private void Update()
    {       
  
        if (tileDestroyTimer <= 10.0f)
        {
            TileDestroyInfo.enabled = true;
            TileDestroyCountdown.enabled = true;
            Canvas.SetActive(true);

            TileDestroyCountdown.text = tileDestroyTimer.ToString();

            if (tileDestroyTimer <= 0.0f)
            {
                DestroyTileProzess();
            }
        }     
        
    }


    private void DestroyTileProzess()
    {
        //remove Tile in Array
        var rnd = Random.Range(1, tileArray.Count);
        var deleteTileName = tileArray[rnd].name;
        tileArray.RemoveAt(rnd);


        //remove tile in the world
        var deletetileObj = GameObject.Find(deleteTileName);
        if (deletetileObj == null)
        {
            Debug.Log("couln't find tile obj"+deletetileObj.name);
        }
        Destroy(deletetileObj);


        tileDestroyTimer = 30;
        TileDestroyInfo.enabled = false;
        TileDestroyCountdown.enabled = false;
        Canvas.SetActive(false);
        

    }


    IEnumerator StartTimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            tileDestroyTimer--;
        }
    }


}